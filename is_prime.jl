function is_prime(n)
    if n <= 1
        return false
    end

    for i = 2:sqrt(n)
        if n % i == 0
            return false
        end
    end
    return true
end

function main()
    n = parse(Int, ARGS[1])
    if is_prime(n)
        println(n, " é primo")
    else
        println(n, " não é primo")
    end
end

main()